.PHONY: all
.PHONY: build start stop down

all: start

dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

docker-compose := $(dir)/docker-compose.sh

build:
	$(docker-compose) build --no-cache

start:
	$(docker-compose) up -d

stop:
	$(docker-compose) stop -t 10

down:
	$(docker-compose) down -t 10 --rmi all
