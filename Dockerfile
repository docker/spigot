FROM alpine:edge
LABEL maintainer="Guillaume Charifi <guillaume.charifi@sfr.fr>"

ENTRYPOINT /spigot/server.sh
CMD []

RUN echo 'http://dl-cdn.alpinelinux.org/alpine/edge/testing' >>/etc/apk/repositories
RUN apk -U add openjdk23-jre-headless
COPY spigot /spigot
