#!/bin/bash

export UID_GID="$(id -u):$(id -g)"

exec docker-compose --env-file env.sh "$@"
